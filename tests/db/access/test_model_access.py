import unittest
from threading import Thread
from time import sleep

from bidon.db.access import ModelAccess, RollbackTransaction, Upsert, autocommit, transaction
from bidon.db.model import ModelBase

from tests import get_model_access


__all__ = ["DbAccessModelAccessTestCase"]


class Person(ModelBase):
  table_name = "people"
  attrs = {
    "first_name": None,
    "last_name": None,
    "age": 0 }


class PersonThing(ModelBase):
  table_name = "peoples_things"
  primary_key_name = ("person_id", "thing_id")
  primary_key_is_auto = False
  attrs = {
    "person_id": None,
    "thing_id": None,
    "quantity": 0}


class Thing(ModelBase):
  table_name = "things"
  attrs = {
    "name": None,
    "type": None,
    "other": None }


class DbAccessModelAccessTestCase(unittest.TestCase):
  @classmethod
  def setUpClass(cls):
    cls.mab = get_model_access()
    cls.mab.open()

  @classmethod
  def tearDownClass(cls):
    cls.mab.rollback()
    cls.mab.close()

  def test_find_model(self):
    p = self.mab.find_model(Person, dict(first_name="Yours"))
    self.assertIsNotNone(p)
    self.assertIsInstance(p, Person)
    self.assertEqual(p.first_name, "Yours")

  def test_find_models(self):
    ps = list(self.mab.find_models(Person, "last_name like 'T%'"))
    self.assertEqual(len(ps), 2)
    self.assertIsInstance(ps[0], Person)
    self.assertEqual(ps[0].last_name[0], "T")

  def test_page_models(self):
    ps, count = self.mab.page_models(Person, (0, 4), order_by="id")
    self.assertEqual(count, 11)
    self.assertEqual(len(ps), 4)
    self.assertEqual([1, 2, 3, 4], [p.id for p in ps])

  def test_find_model_by_id(self):
    pt = self.mab.find_model_by_id(PersonThing, [1, 1])
    self.assertEqual(pt.person_id, 1)
    self.assertEqual(pt.thing_id, 1)
    self.assertEqual(pt.quantity, 1)
    p = self.mab.find_model_by_id(Person, 1)

  def test_refresh_model(self):
    with get_model_access() as mab:
      pt = mab.find_model(Person, "1=1")
      nn = "aabbccdd"
      mab.update(Person.table_name, dict(first_name=nn), dict(id=pt.id))
      self.assertNotEqual(pt.first_name, nn)
      self.assertEqual(mab.refresh_model(pt, overwrite=False).first_name, nn)
      self.assertNotEqual(pt.first_name, nn)
      mab.refresh_model(pt, overwrite=True)
      self.assertEqual(pt.first_name, nn)
      mab.rollback()

  def test_update_model(self):
    with get_model_access() as mab:
      p1 = mab.find_model_by_id(Person, 1)
      ua = p1.updated_at
      self.assertNotEqual(p1.first_name, "Minerva")
      updatedattrs = p1.update(dict(first_name="Minerva", last_name="McGonagall"))
      mab.update_model(p1, include_keys=updatedattrs)
      if mab.core.supports_returning_syntax:
        self.assertNotEqual(ua, p1.updated_at)
      p2 = mab.find_model_by_id(Person, 1)
      self.assertEqual(p2.first_name, "Minerva")
      self.assertEqual(p2.last_name, "McGonagall")
      mab.rollback()

  def test_insert_model(self):
    with get_model_access() as mab:
      p1 = Person(dict(first_name="Albus", last_name="Dumbledore", age=115))
      self.assertIsNone(p1.id)
      mab.insert_model(p1)
      self.assertIsNotNone(p1.id)
      self.assertIsNotNone(p1.created_at)
      p2 = mab.find_model_by_id(Person, p1.id)
      self.assertEqual(p1.first_name, p2.first_name)
      self.assertEqual(p1.last_name, p2.last_name)
      self.assertEqual(p1.age, p2.age)

      tparams = dict(name="Treyfus Cucuss", type="person")
      t1 = Thing(tparams)
      t2 = Thing(tparams)

      # Insert a thing to run upserts against
      self.assertIsNone(t1.id)
      mab.insert_model(t1)
      self.assertIsNotNone(t1.id)

      # Make sure that t2 has no id
      self.assertIsNone(t2.id)

      # Upsert it, but don't force the update, which should result in no ID being assigned.
      mab.insert_model(t2, upsert=Upsert(Upsert.DO_UPDATE, ("name", ), False))
      self.assertIsNone(t2.id)

      # Upsert it, but force the update, so the result should now have an ID, and it should be the
      # same ID as the first.
      mab.insert_model(t2, upsert=Upsert(Upsert.DO_UPDATE, ("name", ), True))
      self.assertIsNotNone(t2.id)
      self.assertEqual(t1.id, t2.id)

      mab.rollback()

  def test_delete_model(self):
    with get_model_access() as mab:
      pc = mab.count("people")
      mab.delete_model(Person(id=11))
      self.assertEqual(pc - 1, mab.count("people"))
      self.assertIsNone(mab.find_model_by_id(Person, 11))

      ptc = mab.count("peoples_things")
      mab.delete_model(PersonThing(person_id=1, thing_id=1))
      self.assertEqual(ptc - 1, mab.count("peoples_things"))
      self.assertIsNone(mab.find_model_by_id(PersonThing, [1, 1]))

      mab.delete_model(PersonThing, [1, 2])
      self.assertEqual(ptc - 2, mab.count("peoples_things"))
      self.assertIsNone(mab.find_model_by_id(PersonThing, [1, 1]))

      mab.rollback()

  def test_find_or_upsert(self):
    class UpsertTest(ModelBase):
      table_name = "upserts"
      attrs = { "name": None, "type": None }

    def insert(ma, name, type):
      return ma.insert_model(UpsertTest(name=name, type=type))

    def upsert(ma, name, type, assign):
      with transaction(ma):
        assign.append(ma.find_or_upsert(UpsertTest, dict(name=name, type=type), comp=dict(name=name), return_status=True))

    with get_model_access() as ma1, get_model_access() as ma2:
      with autocommit(ma1):
        ma1.execute("drop table if exists upserts")
        ma1.execute("create table upserts (id serial not null primary key, name text not null unique, type text not null, created_at timestamptz not null default now(), updated_at timestamptz not null default now());")
        ma1.execute("truncate table upserts")

      # 1) Two transactions: a) create b) upsert a) commit - check that a and b have same id
      # 2) Two transactions: a) create b) upsert a) rollback - check that a and b have differnet ids

      with transaction(ma1):
        mod1 = insert(ma1, "Trey", "person")
        mod2a = []
        thread1 = Thread(target=lambda: upsert(ma2, "Trey", "person", mod2a))
        thread1.start()
        sleep(0.25)
        self.assertTrue(thread1.is_alive())
      thread1.join()
      mod2, mod2_status = mod2a[0]
      self.assertEqual(mod2_status, "duplicate")
      self.assertEqual(mod1.id, mod2.id)

      with transaction(ma1):
        mod3 = insert(ma1, "Julie", "person")
        mod4a = []
        thread2 = Thread(target=lambda: upsert(ma2, "Julie", "person", mod4a))
        thread2.start()
        sleep(0.25)
        self.assertTrue(thread2.is_alive())
        raise RollbackTransaction()
      thread2.join()
      mod4, mod4_status = mod4a[0]
      self.assertEqual(mod4_status, "created")
      self.assertNotEqual(mod3.id, mod4.id)

      mod5a = []
      upsert(ma1, "Trey", "person", mod5a)
      mod5, mod5_status = mod5a[0]
      self.assertEqual(mod5_status, "found")
      self.assertEqual(mod5.id, mod1.id)
