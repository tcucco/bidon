#!/bin/bash

DIR=$(dirname $0)
DEFAULTS_FILE="$HOME/.mysql-defaults/localhost"
DB_NAME="bidon_test"
FILENAME="$DIR/mysql.sql.gz"
CREATE="false"
DROP="false"

usage()
{
  cat <<EOF
Setup SQLITE database for testing

Options:
  -h  Show this help message
  -d  Drop database
  -c  Create database
  -f  Drop and create database

EOF
}

while getopts "hcdf" OPTION; do
  case $OPTION in
    h) usage; exit ;;
    c|f)
      DROP="true"
      CREATE="true"
      ;;
    d) DROP="true" ;;
    ?) usage; exit 1 ;;
  esac
done

if [ "$DROP" == "true" ]; then
  mysql --defaults-file="$DEFAULTS_FILE" -e "drop database if exists $DB_NAME"
fi

if [ "$CREATE" == "true" ]; then
  mysql --defaults-file="$DEFAULTS_FILE" -e "create database $DB_NAME"
  gzip -dc "$FILENAME" | mysql --defaults-file="$DEFAULTS_FILE" "$DB_NAME"
fi
