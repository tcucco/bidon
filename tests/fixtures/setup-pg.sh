#!/bin/bash

DIR=$(dirname $0)
DB_HOST="localhost"
DB_PORT="5432"
DB_USER="postgres"
DB_NAME="bidon_test"
FILENAME="$DIR/pg.sql.gz"
CREATE="false"
DROP="false"

usage()
{
  cat <<EOF
Setup SQLITE database for testing

Options:
  -h  Show this help message
  -d  Drop database
  -c  Create database
  -f  Drop and create database

EOF
}

while getopts "hcdfp:" OPTION; do
  case $OPTION in
    h) usage; exit ;;
    c|f)
      DROP="true"
      CREATE="true"
      ;;
    d) DROP="true" ;;
    p) DB_PORT="$OPTARG" ;;
    ?) usage; exit 1 ;;
  esac
done

if [ "$DROP" == "true" ]; then
  dropdb -h "$DB_HOST" -p "$DB_PORT" -U "$DB_USER" -w "$DB_NAME"
fi

if [ "$CREATE" == "true" ]; then
  createdb -h "$DB_HOST" -p "$DB_PORT" -U "$DB_USER" -w "$DB_NAME"
  gzip -dc "$FILENAME" | psql -h "$DB_HOST" -p "$DB_PORT" "$DB_NAME" "$DB_USER"
fi
