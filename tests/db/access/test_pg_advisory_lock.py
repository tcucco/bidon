import unittest

from bidon.db.access.pg_advisory_lock import LockMode, advisory_lock, lock_key
from bidon.db.access.pg_advisory_lock import obtain_lock, release_lock

from tests import get_data_access, CONFIG


__all__ = ["DbAccessPGAdvisoryLockTestCase"]


class DbAccessPGAdvisoryLockTestCase(unittest.TestCase):
  def test_advisory_lock(self):
    if not CONFIG.is_pg:
      return

    da0 = get_data_access().open()
    da1 = get_data_access().open()
    key = 1

    da0_got = False
    da1_got = False

    with advisory_lock(da0, key, LockMode.skip) as got_lock0:
      self.assertTrue(got_lock0)
      with advisory_lock(da1, key, LockMode.skip) as got_lock1:
        self.assertFalse(got_lock1)

    # The lock should already be released, so trying to release again should
    # return false
    self.assertFalse(release_lock(da0, key))

    with advisory_lock(da0, key, LockMode.skip) as got_lock2:
      self.assertTrue(got_lock2)
      with self.assertRaises(Exception):
        with advisory_lock(da1, key, LockMode.error):
          pass

  def test_lock_key(self):
    if not CONFIG.is_pg:
      return

    self.assertEqual(lock_key(1, 1, 8), (1 << 8) | 1)

    with self.assertRaises(Exception):
      lock_key(1 << 8, 1, 8)

    with self.assertRaises(Exception):
      lock_key(1, 1 << 55)

  def test_obtain_release_lock(self):
    if not CONFIG.is_pg:
      return
    da0 = get_data_access().open()
    da1 = get_data_access().open()

    key = 1

    # Get lock with first connection
    self.assertTrue(obtain_lock(da0, key, LockMode.skip))

    # Try to get lock with second connection, fails
    self.assertFalse(obtain_lock(da1, key, LockMode.skip))

    # Try to release lock with second connection, fails
    self.assertFalse(release_lock(da1, key))

    # Release lock wth first connection
    self.assertTrue(release_lock(da0, key))

    # Get lock with second connection
    self.assertTrue(obtain_lock(da1, key, LockMode.skip))

    # Release lock with second connection
    self.assertTrue(release_lock(da1, key))
