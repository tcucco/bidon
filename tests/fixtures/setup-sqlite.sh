#!/bin/bash

DIR=$(dirname $0)
SQL_FILENAME="$DIR/sqlite3.sql.gz"
DB_FILENAME="$DIR/test.sqlite3"
CREATE="false"
DROP="false"

usage()
{
  cat <<EOF
Setup SQLITE database for testing

Options:
  -h  Show this help message
  -d  Drop database
  -c  Create database
  -f  Drop and create database

EOF
}

while getopts "hcdf" OPTION; do
  case $OPTION in
    h) usage; exit ;;
    c|f)
      DROP="true"
      CREATE="true"
      ;;
    d) DROP="true" ;;
    ?) usage; exit 1 ;;
  esac
done

if [ "$DROP" == "true" ]; then
  if [ -f "$DB_FILENAME" ]; then
    rm "$DB_FILENAME"
  fi
fi

if [ "$CREATE" == "true" ]; then
  gzip -dc "$SQL_FILENAME" | sqlite3 "$DB_FILENAME"
fi
