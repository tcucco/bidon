from datetime import date, time, datetime, timezone, timedelta
import unittest

from bidon.util import date as udate

class UtilDateTestCase(unittest.TestCase):
  def test_parse_date(self):
    exv = date(2016, 1, 15)
    self.assertEqual(udate.parse_date("2016-01-15"), exv)
    self.assertEqual(udate.parse_date("20160115"), exv)
    self.assertEqual(udate.parse_date("01/15/2016", "%m/%d/%Y"), exv)
    self.assertEqual(udate.parse_date("2016-01-15 13:15:00"), exv)
    self.assertEqual(udate.parse_date(exv), exv)

  def test_parse_time(self):
    exv = time(13, 15, 0)
    self.assertEqual(udate.parse_time("13:15:00"), exv)
    self.assertEqual(udate.parse_time("1:15 pm"), exv)
    self.assertEqual(udate.parse_time("1:15 PM"), exv)
    self.assertEqual(udate.parse_time("131500"), exv)
    self.assertEqual(udate.parse_time("131500.0"), exv)
    self.assertEqual(udate.parse_time(exv), exv)

  def test_parse_datetime(self):
    exv1 = datetime(2016, 2, 1, 8, 32, 15)
    exv2 = datetime(2016, 2, 1, 8, 32, 15, tzinfo=timezone.utc)
    exv3 = datetime(2016, 2, 1, 8, 32, 15, tzinfo=timezone(timedelta(hours=-7)))
    exv4 = datetime(2016, 2, 1, 8, 32, 15, tzinfo=timezone(timedelta(hours=5)))
    self.assertEqual(udate.parse_datetime("2016-02-01 08:32:15"), exv1)
    self.assertEqual(udate.parse_datetime("2016-02-01 08:32:15Z"), exv2)
    self.assertEqual(udate.parse_datetime("2016-02-01 08:32:15+0000"), exv2)
    self.assertEqual(udate.parse_datetime("2016-02-01 08:32:15-0700"), exv3)
    self.assertEqual(udate.parse_datetime("2016-02-01 08:32:15-07"), exv3)
    self.assertEqual(udate.parse_datetime("2016-02-01 08:32:15+0500"), exv4)
    self.assertEqual(udate.parse_datetime("2016-02-01 08:32:15+05"), exv4)
    self.assertEqual(udate.parse_datetime(exv1), exv1)
