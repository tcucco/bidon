"""The data package contains classes and methods for workig with DB API 2 databases.
"""
from . import access
from . import core
from . import model
