.. Bidon documentation master file, created by
   sphinx-quickstart on Tue Mar 29 08:06:56 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Bidon's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. automodule:: bidon.data.data_access
   :members:

.. automodule:: bidon.data.data_access_core
   :members:

.. automodule:: bidon.data.foreign_model_wrapper
   :members:

.. automodule:: bidon.data.model_access
   :members:

.. automodule:: bidon.data.model_base
   :members:

.. automodule:: bidon.data.pg_advisory_lock
   :members:

.. automodule:: bidon.data.sql_writer
   :members:

