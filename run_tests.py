import argparse
import logging

def main():
  args = parse_args()
  run_tests(args)


def parse_args():
  test_parser = argparse.ArgumentParser("Run the Bidon test suite.")
  test_parser.add_argument("-f", "--fail-fast", default=False, action="store_true")
  test_parser.add_argument("-v", "--verbose", default=1, action="count")
  test_parser.add_argument("-d", "--database", default="postgres", choices={"postgres", "mysql", "sqlite"})
  test_parser.add_argument("-l", "--log", default=False, action="store_true")
  test_parser.add_argument("-p", "--port", default=None)
  test_parser.add_argument("test_name", default=None, nargs="?")
  return test_parser.parse_args()


def run_tests(args):
  import tests
  import unittest

  tests.configure(args)

  if args.log:
    logger = logging.getLogger("bidon.db.access.data_access")
    logger.setLevel(logging.DEBUG)
    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)
    logger.addHandler(sh)

  if args.test_name:
    suite = unittest.TestLoader().loadTestsFromName(args.test_name, module=tests)
  else:
    suite = unittest.TestLoader().loadTestsFromModule(tests)

  unittest.TextTestRunner(verbosity=args.verbose, failfast=args.fail_fast).run(suite)


if __name__ == "__main__":
  main()
