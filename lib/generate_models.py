"""This script turns ouput from an INFORMATION_SCHEMA query into Python source for bidon.ModelBases.
"""
import csv
import os
import sys
from argparse import ArgumentParser
from collections import OrderedDict

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

from bidon.util import get_file_object
from bidon.data_table import DataTable


def main():
  """Function called when script is called from command line."""
  args = parse_args()
  args.func(args)


def parse_args():
  """Parses command line arguments."""
  parser = ArgumentParser(description="ModelBase builder")
  subparsers = parser.add_subparsers()

  sql_parser = subparsers.add_parser(
    "get-query",
    description="Usage: e.g. psql -c \"copy ($(python3 lib/generate_models.py get-query)) to " +
                "stdout with csv header\" DB_NAME postgres")
  sql_parser.set_defaults(func=print_sql_query)

  gen_parser = subparsers.add_parser("generate")
  gen_parser.add_argument("filename", nargs="?", help="Read this file for input, or STDIN if not " \
                                                      "given")
  gen_parser.add_argument("-i", "--indent", default="  ")
  gen_parser.add_argument("-c", "--created-at-col-name", default="created_at")
  gen_parser.add_argument("-u", "--updated-at-col-name", default="updated_at")
  gen_parser.set_defaults(func=generate_models)

  args = parser.parse_args()

  if hasattr(args, "func"):
    return args
  else:
    arg_parser.print_help()
    sys.exit(1)


def print_sql_query(args):
  """Method used by the get-query subcommand. Prints the Postgres SQL string that will generate the
  output the script needs."""
  print(_PG_SQL)


def generate_models(args):
  """Generates models from the script input."""
  data_table = get_data_table(args.filename)
  tables = to_tables(data_table.rows_to_dicts())
  attr_indent = "\n" + args.indent * 2
  attr_sep = "," + attr_indent

  for tname, cols in tables.items():
    model_name = table_to_model_name(tname, list(cols.values())[0]["table_schema"])
    pk_cols, oth_cols = split_pks(cols)
    timestamps = get_timestamps(cols, args.created_at_col_name, args.updated_at_col_name)
    is_auto = len(pk_cols) == 1 and cols[pk_cols[0]]["is_auto"] == "t"

    attrs = OrderedDict()
    for cname in oth_cols:
      if cname not in timestamps:
        attrs[cname] = None

    print(_MODEL_SOURCE.format(
      class_name=model_name,
      base_class_name="ModelBase",
      indent=args.indent,
      table_name=repr(tname),
      pk_name=repr(pk_cols[0] if len(pk_cols) == 1 else pk_cols),
      pk_is_auto=is_auto,
      timestamps=timestamps,
      attrs="dict(" + attr_indent + attr_sep.join("{0}={1}".format(k, v) for k, v in attrs.items()) + ")"))
    print()


def get_data_table(filename):
  """Returns a DataTable instance built from either the filename, or STDIN if filename is None."""
  with get_file_object(filename, "r") as rf:
    return DataTable(list(csv.reader(rf)))


def to_tables(cols):
  """Builds and returns a Dictionary whose keys are table names and values are OrderedDicts whose
  keys are column names and values are the col objects from which the definition is derived.
  """
  tables = OrderedDict()
  for col in cols:
    tname = col["table_name"]
    if tname not in tables:
      tables[tname] = OrderedDict()
    tables[tname][col["column_name"]] = col
  return tables


def table_to_model_name(name, schema):
  """Converts a table name to a model name."""
  return snake_to_pascal(name, singularize=True)


def snake_to_pascal(name, singularize=False):
  """Converts snake_case to PascalCase. If singularize is True, an attempt is made at singularizing
  each part of the resulting name.
  """
  parts = name.split("_")
  if singularize:
    return "".join(p.upper() if p in _ALL_CAPS else to_singular(p.title()) for p in parts)
  else:
    return "".join(p.upper() if p in _ALL_CAPS else p.title() for p in parts)


def to_singular(word):
  """Attempts to singularize a word."""
  if word[-1] != "s":
    return word
  elif word.endswith("ies"):
    return word[:-3] + "y"
  elif word.endswith("ses"):
    return word[:-2]
  else:
    return word[:-1]


def split_pks(cols):
  """Returns a 2-tuple of tuples of ((primary_key_cols), (non_primary_key_cols))."""
  pks = []
  others = []
  for name, col in cols.items():
    if col["is_primary_key"] == "t":
      pks.append(name)
    else:
      others.append(name)

  return (tuple(pks), tuple(others))


def get_timestamps(cols, created_name, updated_name):
  """Returns a 2-tuple of the timestamp columns that were found on the table definition."""
  has_created = created_name in cols
  has_updated = updated_name in cols
  return (created_name if has_created else None, updated_name if has_updated else None)


_ALL_CAPS = set()

_PG_SQL = """with primary_key_cols as (
  select tc.table_schema, tc.table_name, tc.constraint_name, ccu.column_name
  from information_schema.table_constraints as tc
       inner join information_schema.constraint_column_usage as ccu
         on (tc.table_schema, tc.table_name, tc.constraint_name)
            = (ccu.table_schema, ccu.table_name, ccu.constraint_name)
  where tc.constraint_type = 'PRIMARY KEY'
)
select c.table_schema, c.table_name, c.ordinal_position, c.column_name, c.column_default,
       c.is_nullable, c.data_type, c.character_maximum_length,
       pk.table_schema is not null as is_primary_key,
       column_default ilike 'nextval(%)' as is_auto
from information_schema.columns as c
     left join primary_key_cols as pk
       on (c.table_schema, c.table_name, c.column_name)
          = (pk.table_schema, pk.table_name, pk.column_name)
where c.table_schema in ('public')
order by c.table_schema, c.table_name, c.ordinal_position"""

_MODEL_SOURCE = """class {class_name}({base_class_name}):
{indent}\"\"\"Mirrors the {table_name} table.\"\"\"
{indent}table_name = {table_name}
{indent}primary_key_name = {pk_name}
{indent}primary_key_is_auto = {pk_is_auto}
{indent}timestamps = {timestamps}
{indent}attrs = {attrs}
"""


if __name__ == "__main__":
  main()
