"""Contains tests for xml stream writing service classes."""
from io import StringIO
from unittest import TestCase

from bidon.xml import StreamWriter


class XMLStreamWriterTestCase(TestCase):
  """Test case for the xml.StreamWriter service class."""
  def test_output(self):
    output = StringIO()
    expected = """<?xml version="1.0" encoding="UTF-8"?>
<project id="12345" name="Trey's Project" xmlns:ns="www.aecinsights.com/xmlschemas/export_v1.0">
  <territory id="18">
    Southwest
  </territory>
</project>
"""

    with StreamWriter(output, indent="  ") as xsw:
      xsw.open("project", {"name": "Trey's Project", "id": "12345", ("xmlns", "ns"): "www.aecinsights.com/xmlschemas/export_v1.0"})
      xsw.open("territory", {"id": 18})
      xsw.characters("Southwest")

    output.seek(0)
    self.assertEqual(output.read(), expected)
